# Bài tập 2

## Tên bài tập: Xây dựng ứng dụng quản lý khách hàng
### Tạo Project Quản Lý Bán Hàng trên VisualStudio dạng Asp.Net Core, thực hiện yêu cầu quản lý thông tin của một chi nhánh bán hàng, hãy thực hiện theo các yêu cầu sau.

**a. Tạo các Model định nghĩa các thuộc tính đại diện cho thông tin cơ bản của một chi nhánh bán hàng giả tưởng. Các thuộc tính như sau:**

|     Tên thuộc tính    |     Kiểu dữ liệu    |                 Mô tả               |
|:---------------------:|:-------------------:|:-----------------------------------:|
|        BranchId       |          Int        |     Mã chi nhánh                    |
|          Name         |        String       |     Tên chi nhánh                   |
|         Address       |        String       |     Địa chỉ                         |
|          City         |        String       |     Thành phố đặt chi nhánh         |
|          State        |        String       |     Trạng thái: Active, DeActive    |
|         ZipCode       |        String       |     Mã bưu chính                    |

**b. Tạo BranchesController:**
    Các hàm và Action trong BranchesController: 
- Constructor, 
- Add, 
- Edit, 
- Details, 
- Get, 
- Remove. 

**c. Viết API lấy toàn bộ dữ liệu của branch**

**d. Viết API Thêm branch**

**e. Viết API Xóa branch**

**f. Viết API sửa branch**

Sample: https://learn.microsoft.com/vi-vn/aspnet/core/tutorials/first-web-api?view=aspnetcore-8.0&tabs=visual-studio 

## Nội dung