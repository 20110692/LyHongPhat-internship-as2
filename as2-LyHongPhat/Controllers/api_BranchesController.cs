﻿using as2_LyHongPhat.Data;
using as2_LyHongPhat.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace as2_LyHongPhat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class api_BranchesController : ControllerBase
    {
        private readonly as2_LyHongPhatContext _context;

        public api_BranchesController(as2_LyHongPhatContext context)
        {
            _context = context;
        }

        // GET: api/api_Branches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Branch>>> GetBranch()
        {
            if (_context.Branch == null)
            {
                return NotFound();
            }
            return await _context.Branch.ToListAsync();
        }

        // GET: api/api_Branches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Branch>> GetBranch(int id)
        {
            if (_context.Branch == null)
            {
                return NotFound();
            }
            var branch = await _context.Branch.FindAsync(id);

            if (branch == null)
            {
                return NotFound();
            }

            return branch;
        }

        // PUT: api/api_Branches/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBranch(int id, Branch branch)
        {
            if (id != branch.BranchID)
            {
                return BadRequest();
            }

            _context.Entry(branch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BranchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/api_Branches
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Branch>> PostBranch(Branch branch)
        {
            if (_context.Branch == null)
            {
                return Problem("Entity set 'as2_LyHongPhatContext.Branch'  is null.");
            }
            _context.Branch.Add(branch);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBranch", new { id = branch.BranchID }, branch);
        }

        // DELETE: api/api_Branches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBranch(int id)
        {
            if (_context.Branch == null)
            {
                return NotFound();
            }
            var branch = await _context.Branch.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }

            _context.Branch.Remove(branch);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BranchExists(int id)
        {
            return (_context.Branch?.Any(e => e.BranchID == id)).GetValueOrDefault();
        }
    }
}