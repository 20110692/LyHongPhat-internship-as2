﻿using System.ComponentModel.DataAnnotations;

namespace as2_LyHongPhat.Models
{
    public class Branch
    {
        public int BranchID { get; set; }

        // Mã chi nhánh
        [DataType(DataType.Text)]
        [Required]
        public string? Name { get; set; }

        // Tên chi nhánh
        [DataType(DataType.Text)]
        public string? Address { get; set; }

        // Địa chỉ
        [DataType(DataType.Text)]
        public string? City { get; set; }

        //Thành phố
        [DataType(DataType.Text)]
        public string? State { get; set; }

        // Trạng thái: Active, DeActive
        [DataType(DataType.PostalCode)]
        public string? ZipCode { get; set; }

        // Mã bưu chính
    }
}