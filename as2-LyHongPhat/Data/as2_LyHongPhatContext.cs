﻿using Microsoft.EntityFrameworkCore;

namespace as2_LyHongPhat.Data
{
    public class as2_LyHongPhatContext : DbContext
    {
        public as2_LyHongPhatContext(DbContextOptions<as2_LyHongPhatContext> options)
            : base(options)
        {
        }

        public DbSet<as2_LyHongPhat.Models.Branch> Branch { get; set; } = null!;
    }
}